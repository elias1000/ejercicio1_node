import express, { Express, Request, Response } from "express";
import dotenv from 'dotenv';

// Configuration the .env file
dotenv.config();

//create express app
const app: Express= express();
const port: string | number = process.env.PORT || 8000;


//Define route the APP
app.get('/', (req: Request, res: Response)=> {
    
    
    
    res.json({
        data: 
            {message: 'Goodbye, world'}
        
    });
});


//Define route the APP
app.get('/hello/:nombre?', (req: Request, res: Response)=> {
    //send hello world
    let valor_nombre= req.params.nombre;
    if (valor_nombre==undefined){
        valor_nombre= 'anonimo'
    }
    res.json({
        data: 
            {message: `hola ${valor_nombre}`}
        
    });

});


//execute app and listen requests to PORT

app.listen(port, ()=>{
    console.log(`Express server: running at http://localhost:${port}`)
})